<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;

class UrlSafetyCheckerService
{
    public function isUrlSafe(string $url): bool
    {
        /**
         * Check if the given URL is safe using Google's Safe Browsing API.
         *
         * @param string $url The URL to check.
         * @return bool True if the URL is safe, false otherwise.
         */

        $apiKey = config('app.google_api_key');

        $response = Http::post('https://safebrowsing.googleapis.com/v4/threatMatches:find?key=' . $apiKey, [
            'client' => [
                'clientId' => 'url-shortener',
                'clientVersion' => '1.0',
            ],
            'threatInfo' => [
                'threatTypes' => ['MALWARE', 'SOCIAL_ENGINEERING'],
                'platformTypes' => ['ANY_PLATFORM'],
                'threatEntryTypes' => ['URL'],
                'threatEntries' => [
                    ['url' => $url]
                ],
            ],
        ]);

        return !$response->successful() || empty($response->json('matches'));
    }
}

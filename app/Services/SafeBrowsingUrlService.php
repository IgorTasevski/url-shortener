<?php

namespace App\Services;

use App\Repositories\Interfaces\UrlRepositoryInterface;
use App\Traits\HandleDbTransactionTrait;
use App\Traits\ResponseTrait;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Http;

/**
 * Class SafeBrowsingUrlService
 *
 * Service for handling URL shortening with Google's Safe Browsing API.
 */
class SafeBrowsingUrlService
{
    use ResponseTrait, HandleDbTransactionTrait;

    /**
     * SafeBrowsingUrlService constructor.
     *
     * @param UrlRepositoryInterface $urlRepository
     */
    public function __construct(
        private readonly UrlRepositoryInterface $urlRepository,
        private readonly UrlSafetyCheckerService $urlSafetyCheckerService
    )
    {
        //
    }

    /**
     * Shorten a given URL.
     *
     * This method will shorten the provided URL using Google's Safe Browsing API.
     * It will first check if the URL is safe. If it is not, it will return an error response.
     * If the URL is safe, it will check if it already exists in the database.
     * If it does, it will return the existing short URL.
     * If it does not, it will create a new short URL and return it.
     *
     * @param string $url The URL to shorten.
     * @return string|JsonResponse The shortened URL or an error response.
     */
    public function shortenUrl(string $url): string|JsonResponse
    {
        // Check if the URL already exists in the database
        $urlExists = $this->urlRepository->checkIfUrlExists($url);

        // If the URL already exists, return the existing short URL
        if ($urlExists) {
            //return the short url with the message
            $shortUrl = $urlExists->base_url . '/' . $urlExists->short_hash;
            $originalUrl = $urlExists->original_url;
            return $this->errorResponse("URL already exists. Short url:${shortUrl}, original url:${originalUrl}", 422,);
        }

        return $this->handleDbTransaction(function () use ($url) {
            $apiKey = config('app.google_api_key');

            // Parse the input URL
            $parsedUrl = parse_url($url);
            $scheme = $parsedUrl['scheme'] ?? 'http';
            $host = $parsedUrl['host'];
            $basePath = $parsedUrl['path'] ?? '';

            // Extract the base URL and path
            $baseUrl = $scheme . '://' . $host;
            $pathSegments = explode('/', trim($basePath, '/'));
            $pathToUrl = implode('/', $pathSegments);


            // Check if the URL is safe
            if (!$this->urlSafetyCheckerService->isUrlSafe($url)) {
                return $this->errorResponse('URL is unsafe', 422);
            }

            // Generate a unique hash for the new URL
            $hash = $this->urlRepository->generateUniqueHash();

            $this->urlRepository->create([
                'original_url' => $url,
                'base_url' => $baseUrl,
                'path_to_url' => $pathToUrl,
                'short_hash' => $hash,
            ]);

            // Construct the short URL
            $shortUrl = $baseUrl;
            if (!empty($pathToUrl)) {
                $shortUrl .= '/' . $pathToUrl;
            }
            $shortUrl .= '/' . $hash;

            return $shortUrl;
        });
    }
}

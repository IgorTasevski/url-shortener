<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUrlRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'url' => 'required|url:http,https'
        ];
    }

    public function messages()
    {
        return [
            'url.required' => 'The URL field is required.',
            'url.url' => 'The URL format is invalid.',
            'url.http' => 'The URL must start with http or https.',
            'url.https' => 'The URL must start with http or https.'
        ];
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUrlRequest;
use App\Http\Resources\UrlResource;
use App\Models\Url;
use App\Repositories\Interfaces\UrlRepositoryInterface;
use App\Services\SafeBrowsingUrlService;
use App\Traits\ResponseTrait;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;

class UrlController extends Controller
{
    use ResponseTrait;
    public function __construct(
        private readonly SafeBrowsingUrlService $safeBrowsingUrlService,
        private readonly UrlRepositoryInterface $urlRepository
    )
    {
    }

    /**
     * @return JsonResponse
     */
    public function getAllUrls(): JsonResponse
    {
        return $this->successResponse(UrlResource::collection($this->urlRepository->getAll()));
    }

    /**
     * @param StoreUrlRequest $request
     * @return JsonResponse
     */
    public function store(StoreUrlRequest $request)
    {
        $data = $request->validated();

        $url = $this->safeBrowsingUrlService->shortenUrl($data['url']);

        // Check if $url is a JsonResponse and contains an error
        if ($url instanceof JsonResponse) {
            $originalContent = $url->getOriginalContent();
            if (isset($originalContent->error) && $originalContent->error) {
                // Extract the error message and status code
                $errorMessage = $originalContent->message;
                $statusCode = $url->getStatusCode();

                // Return the error response
                return $this->errorResponse($errorMessage, $statusCode);
            }
        }

        return $this->successResponse($url, 'URL shortened successfully');
    }

    public function destroy(string $uuid): JsonResponse
    {
        $url = $this->urlRepository->destroyByUuid($uuid);

        if (!$url) {
            return $this->errorResponse('URL not found', 404);
        }

        return $this->successResponse([], 'URL deleted successfully');
    }

    public function show(string $uuid): JsonResponse
    {
        $url = $this->urlRepository->getByUuid($uuid);

        if (!$url) {
            return $this->errorResponse('URL not found', 404);
        }

        return $this->successResponse(new UrlResource($url));
    }

    /**
     * @param string $shortHash
     * @return Application|\Illuminate\Foundation\Application|JsonResponse|RedirectResponse|Redirector
     */
    public function redirectToOriginalUrl(string $shortHash)
    {
        $url = $this->urlRepository->getByShortHash($shortHash);

        if (!$url) {
            return $this->errorResponse('URL not found', 404);
        }

        return redirect($url->original_url);
    }
}

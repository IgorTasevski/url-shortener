<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class UrlResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'uuid' => $this->uuid,
            'original_url' => $this->original_url,
            // short_url is an accessor in the model
            'short_url' => $this->short_url,
            'created_at' => $this->created_at,
        ];
    }
}

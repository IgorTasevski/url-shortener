<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Url extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * @var string[]
     */
    protected $fillable = [
        'uuid',
        'original_url',
        'base_url',
        'path_to_url',
        'short_hash',
    ];

    protected $casts = [
        'created_at' => 'datetime',
    ];

    /**
     * @return string
     */
    public function getShortUrlAttribute(): string
    {
        return $this->base_url . '/' . $this->short_hash;
    }

}

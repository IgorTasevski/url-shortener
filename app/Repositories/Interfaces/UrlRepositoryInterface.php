<?php

namespace App\Repositories\Interfaces;

use App\Models\Url;
use Illuminate\Database\Eloquent\Collection;

interface UrlRepositoryInterface
{
    /**
     * @return Collection
     */
    public function getAll(): Collection;

    /**
     * @return Collection
     */
    public function getAllForAdmin(): Collection;

    /**
     * @param array $data
     * @return Url|null
     */
    public function create(array $data): ?Url;

    /**
     * @param string $url
     * @return Url|null
     */
    public function checkIfUrlExists(string $url): ?Url;

    /**
     * @param string $shortHash
     * @return Url|null
     */
    public function getByShortHash(string $shortHash): ?Url;

    /**
     * @return string
     */
    public function generateUniqueHash(): string;

    /**
     * @param int $id
     * @param array $data
     * @return Url|null
     */
    public function update(int $id, array $data): ?Url;

    /**
     * @param string $uuid
     * @param array $data
     * @return Url|null
     */
    public function updateByUuid(string $uuid, array $data): ?Url;

    /**
     * @param array|int $id
     * @return int
     */
    public function destroyById(array|int $id): int;

    /**
     * @param array|string $uuid
     * @return string
     */
    public function destroyByUuid(array|string $uuid): string;

}

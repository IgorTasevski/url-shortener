<?php

namespace App\Repositories;

use App\Models\Url;
use App\Repositories\Abstracts\AbstractBaseRepository;
use App\Repositories\Interfaces\UrlRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Str;

class UrlRepository
    extends AbstractBaseRepository
    implements UrlRepositoryInterface
{

    /**
     * @return string
     */
    protected function model(): string
    {
        return Url::class;
    }

    /**
     * @var Url $model;
     */
    protected $model;

    /**
     * @return Collection
     */
    public function getAllForAdmin(): Collection {
        // get all records including soft deleted if there's an admin in the future
        return $this->model::withTrashed()->get();
    }

    /**
     * @return Collection
     */
    public function getAll(): Collection {
        return parent::getAll();
    }

    /**
     * @param string $uuid
     * @return Url|null
     */
    public function getByUuid(string $uuid): ?Url {
        return $this->model->where('uuid', $uuid)->first();
    }

    /**
     * @param array $data
     * @return Url|null
     */
    public function create(array $data): ?Url {
        // Generate a UUID for the new record
        $data['uuid'] = Str::uuid();
        return parent::create($data);
    }

    /**
     * @param string $url
     * @return Url|null
     */
    public function checkIfUrlExists(string $url): ?Url
    {
        return $this->model->where('original_url', $url)->first();
    }

    /**
     * @return string
     */
    public function generateUniqueHash(): string
    {
        $hash = Str::random(6);
        while ($this->model->where('short_hash', $hash)->exists()) {
            $hash = Str::random(6);
        }
        return $hash;
    }

    /**
     * @param string $shortHash
     * @return Url|null
     */
    public function getByShortHash(string $shortHash): ?Url {
        return $this->model->where('short_hash', $shortHash)->first();
    }

    /**
     * @param array|int $id
     * @return int
     */
    public function destroyById(array|int $id): int {
        return parent::destroy($id);
    }

    /**
     * @param array|string $uuid
     * @return string
     */
    public function destroyByUuid(array|string $uuid): string {
        $application = $this->getByUuid($uuid);
        return $this->destroy($application->id);
    }

    /**
     * @param int $id
     * @param array $data
     * @return Url|null
     */
    public function update(int $id, array $data): ?Url {
        //Prevent any updates to the uuid
        if (isset($data['uuid'])) {
            unset($data['uuid']);
        }
        return parent::update($id, $data);
    }

    /**
     * @param string $uuid
     * @param array $data
     * @return Url|null
     */
    public function updateByUuid(string $uuid, array $data): ?Url {
        $application = $this->getByUuid($uuid);
        return $this->update($application->id, $data);
    }
}

# URL Shortening Application

This project is a web application built with PHP, JavaScript, and the Laravel framework. It was initiated as a basic Laravel project and has since had several new features implemented.

## Database Schema

The database for this project is structured to support the Urls Repository feature. The main table in the database is the `urls` table, which stores data related to the urls.

The `urls` table includes the following fields:

- `id`: (id)A unique identifier for each URL.
- `uuid`: (uuid) A universally unique identifier for each URL.
- `original_url`: (string) The original URL that was shortened.
- `base_url`: (string) The base URL for the shortened URL.
- `path_to_url`: (string) The path/folders after the domain.
- `short_hash`: (string) A 6 character string alphanumeric to be appended to the `base_url`.
- `created_at`: (timestamp) The date and time when the URL was created.
- `updated_at`: (timestamp) The date and time when the URL was last updated.
- `deleted_at`: (timestamp - soft delete) The date and time when the URL was deleted (if it was deleted).

## There is also a urls.sql file in the root directory of the project that can be used to IMPORT data into the `urls` tables.
This is to be done after running `php artisan migrate.`

Please refer to the Laravel migration files for the exact structure and data types of these fields.

## New Features

### URL Repository

The `UrlRepository` is an interface for interacting with URL data in the database. It includes methods for getting URLs by their short hash, checking if a URL exists, generating a unique hash, and CRUD operations.

The methods included in the `UrlRepositoryInterface` are:

- `getAll()`: Returns a collection of all URLs.
- `getAllForAdmin()`: Returns a collection of all URLs for a potential admin with soft deleted URLs.
- `create(array $data)`: Creates a new URLs with the given data. Returns the created URL or null.
- `checkIfUrlExists(string $url)`: Checks if a URL already exists in the database. Returns the URL or null.
- `getByShortHash(string $shortHash)`: Gets the URL with the given short hash. Returns the URL or null.
- `generateUniqueHash()`: Generates a unique short hash for a new URL. Returns the short hash.
- `update(int $id, array $data)`: Updates the URLs with the given ID. Returns the updated URL or null. //but this is kind of useless, merely there for educational purposes
- `updateByUuid(string $uuid, array $data)`: Updates the URL with the given UUID. Returns the updated URL or null. //useless as the aforementioned. I merely wanted to demonstrate how the CRUD would work
- `destroyById(array|int $id)`: Deletes the URL(s) with the given ID(s). Returns the number of deleted URL. //tested only via postman
- `destroyByUuid(array|string $uuid)`: Deletes the URL(s) with the given UUID(s). Returns the number of deleted URLs.

### Repository binding

In `config/app.php` under `providers` the `App\Providers\RepositoryServiceProvider::class` was added thus allowing us to bind the `Model`RepositoryInterface with the `Model`Repository.

### Routes

The routes are in `api.php`.

### Requests

A custom Request for storing the urls was created called `StoreUrlRequest.php`.

## Installation

1. Clone the repository
2. Run `composer install` to install the PHP dependencies
3. Run `npm install` to install the JavaScript dependencies
4. Set up your `.env` file with your database and other environment settings. Copy the `.env.example` file and rename it to `.env` - then fill in the necessary values with your own
5. Run `php artisan migrate` to create the database tables
6. Run `php artisan serve` to start the Laravel server
7. In a new terminal, navigate to the `url-shortener` directory and run `npm run dev` to start the Vite server for the frontend
8. Open your browser and navigate to `http://localhost:3000` (or whatever port your Vite server is running on)
9. I used [Laravel Herd](https://herd.laravel.com/windows) for this project so the `vite.config.js` file is a bit different

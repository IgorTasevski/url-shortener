<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>URL Shortener</title>
    @vite('resources/js/app.js')
    @vite('resources/css/app.css')
</head>
<body>
<div id="app">
    <url-form></url-form>
</div>
</body>
</html>

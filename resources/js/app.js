import { createApp } from 'vue';
import UrlForm from './components/UrlForm.vue';
import '../css/urlForm.css';
import Toast from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-sugar.css';

const app = createApp(UrlForm);
app.use(Toast);
app.mount('#app');


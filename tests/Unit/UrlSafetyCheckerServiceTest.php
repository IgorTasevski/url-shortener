<?php

namespace Tests\Unit;

use App\Services\UrlSafetyCheckerService;
use Illuminate\Support\Facades\Http;
use Tests\TestCase;

class UrlSafetyCheckerServiceTest extends TestCase
{
    public function testUrlIsSafe()
    {
        Http::fake([
            'https://safebrowsing.googleapis.com/v4/threatMatches:find*' => Http::response([], 200)
        ]);

        $service = new UrlSafetyCheckerService();
        $this->assertTrue($service->isUrlSafe('http://example.com'));
    }

    public function testUrlIsUnsafe()
    {
        Http::fake([
            'https://safebrowsing.googleapis.com/v4/threatMatches:find*' => Http::response(['matches' => ['threats']], 200)
        ]);

        $service = new UrlSafetyCheckerService();
        $this->assertFalse($service->isUrlSafe('http://example.com'));
    }
}

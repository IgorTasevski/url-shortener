<?php

namespace Tests\Unit;

use App\Repositories\Interfaces\UrlRepositoryInterface;
use App\Services\UrlSafetyCheckerService;
use App\Services\SafeBrowsingUrlService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\JsonResponse;
use Tests\TestCase;
use Mockery;

class SafeBrowsingUrlServiceTest extends TestCase
{
    use RefreshDatabase;

    public function testShortenUrlAlreadyExists()
    {
        $urlRepositoryMock = Mockery::mock(UrlRepositoryInterface::class);
        $urlSafetyCheckerServiceMock = Mockery::mock(UrlSafetyCheckerService::class);

        $urlRepositoryMock->shouldReceive('checkIfUrlExists')
        ->once()
        ->with('http://example.com')
        ->andReturn((object)[
        'base_url' => 'http://short.ly',
        'short_hash' => 'abc123',
        'original_url' => 'http://example.com'
        ]);

        $service = new SafeBrowsingUrlService($urlRepositoryMock, $urlSafetyCheckerServiceMock);

        $response = $service->shortenUrl('http://example.com');

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals(422, $response->getStatusCode());
        $this->assertStringContainsString('URL already exists', $response->getContent());
    }

    public function testShortenUrlIsUnsafe()
    {
        $urlRepositoryMock = Mockery::mock(UrlRepositoryInterface::class);
        $urlSafetyCheckerServiceMock = Mockery::mock(UrlSafetyCheckerService::class);

        $urlRepositoryMock->shouldReceive('checkIfUrlExists')
            ->once()
            ->with('http://example.com')
            ->andReturn(null);

        $urlSafetyCheckerServiceMock->shouldReceive('isUrlSafe')
            ->once()
            ->with('http://example.com')
            ->andReturn(false);

        $service = new SafeBrowsingUrlService($urlRepositoryMock, $urlSafetyCheckerServiceMock);

        $response = $service->shortenUrl('http://example.com');

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals(422, $response->getStatusCode());
        $this->assertStringContainsString('URL is unsafe', $response->getContent());
    }

    public function testShortenUrlSuccess()
    {
        $urlRepositoryMock = Mockery::mock(UrlRepositoryInterface::class);
        $urlSafetyCheckerServiceMock = Mockery::mock(UrlSafetyCheckerService::class);

        $urlRepositoryMock->shouldReceive('checkIfUrlExists')
            ->once()
            ->with('http://example.com')
            ->andReturn(null);

        $urlSafetyCheckerServiceMock->shouldReceive('isUrlSafe')
            ->once()
            ->with('http://example.com')
            ->andReturn(true);

        $urlRepositoryMock->shouldReceive('generateUniqueHash')
            ->once()
            ->andReturn('uniquehash');

        $urlRepositoryMock->shouldReceive('create')
        ->once()
        ->andReturn(null);

        $service = new SafeBrowsingUrlService($urlRepositoryMock, $urlSafetyCheckerServiceMock);

        $shortUrl = $service->shortenUrl('http://example.com');

        $this->assertIsString($shortUrl);
        $this->assertStringContainsString('http://example.com', $shortUrl);
        $this->assertStringContainsString('uniquehash', $shortUrl);
    }

    protected function tearDown(): void
    {
        Mockery::close();
        parent::tearDown();
    }
}

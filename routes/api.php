<?php

use App\Http\Controllers\UrlController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::prefix('url')
    ->name('url.')
    ->controller(UrlController::class)
    ->group(function () {
        Route::get('/', 'getAllUrls')->name('getAllUrls');
        Route::get('/{uuid}', 'show')->name('show');
        Route::post('/', 'store')->name('store');
        Route::put('/{uuid}', 'update')->name('update');
        Route::delete('/{uuid}', 'destroy')->name('destroy');
    });
